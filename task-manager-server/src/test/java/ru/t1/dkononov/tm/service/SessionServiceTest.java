package ru.t1.dkononov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.dto.model.SessionDTO;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.marker.UnitCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.model.Session;
import ru.t1.dkononov.tm.service.dto.SessionDTOService;
import ru.t1.dkononov.tm.service.model.SessionService;

import static ru.t1.dkononov.tm.constant.TestData.SESSION;
import static ru.t1.dkononov.tm.constant.TestData.USER1;

@Category(UnitCategory.class)
public class SessionServiceTest extends AbstractSchemaTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final SessionDTOService service = new SessionDTOService(connectionService);

    @Before
    public void before() throws UserIdEmptyException, ProjectNotFoundException, LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        service.add(SESSION);
    }

    @After
    public void after() throws UserIdEmptyException {
        service.clear(USER1.getId());
    }

    @Test
    public void add() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(service.add(SESSION));
        Assert.assertThrows(Exception.class, () -> service.add(USER1.getId(),(SessionDTO) null));
    }

    @Test
    public void addByUserId() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(service.add(USER1.getId(), SESSION));
    }

    @Test
    public void createByUserId() {
        Assert.assertEquals(SESSION.getUserId(), USER1.getId());
    }

    @Test
    public void findAllNull() {
        @NotNull final SessionService sessionService = new SessionService(connectionService);
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    public void updateByNullId() {
        Assert.assertThrows(Exception.class, () -> service.findById(USER1.getId(), null));
    }

}
