package ru.t1.dkononov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkononov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.dkononov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.dto.model.UserDTO;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.field.AbstractFieldException;
import ru.t1.dkononov.tm.exception.field.LoginEmptyException;
import ru.t1.dkononov.tm.marker.DataCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.model.User;
import ru.t1.dkononov.tm.repository.dto.ProjectDTORepository;
import ru.t1.dkononov.tm.repository.dto.SessionDTORepository;
import ru.t1.dkononov.tm.repository.dto.UserDTORepository;
import ru.t1.dkononov.tm.repository.model.ProjectRepository;
import ru.t1.dkononov.tm.repository.model.UserRepository;
import ru.t1.dkononov.tm.service.ConnectionService;
import ru.t1.dkononov.tm.service.PropertyService;
import ru.t1.dkononov.tm.util.HashUtil;

import javax.persistence.EntityManager;

import static ru.t1.dkononov.tm.constant.TestData.*;


@Category(DataCategory.class)
public class UserRepositoryTest extends AbstractSchemaTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    private final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);


    @NotNull
    private final UserDTORepository repository = new UserDTORepository(entityManager);

    @NotNull
    private static final String LOGIN_TEST = "logintest";

    @NotNull
    private static final String PASS_TEST = "logintest";

    @NotNull
    private static final String PASS_RETEST = "logintest";

    @NotNull
    private static final String NAME = "firstName";

    @NotNull
    private UserDTO userTesting;

    @Before
    public void before() throws AbstractException, LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final UserDTORepository repository = new UserDTORepository(entityManager);
            repository.add(USER1);
            repository.add(USER2);
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin(LOGIN_TEST);
            user.setPasswordHash(HashUtil.salt(propertyService, PASS_TEST));
            user.setRole(Role.USUAL);
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final UserDTORepository repository = new UserDTORepository(entityManager);
            @NotNull final UserDTO user1 = new UserDTO();
            user1.setLogin(LOGIN);
            user1.setPasswordHash(HashUtil.salt(propertyService, PASSWORD));
            user1.setRole(Role.USUAL);
            repository.add(user1);
            @Nullable final UserDTO user = repository.findByLogin(LOGIN);
            Assert.assertNotNull(user);
            projectRepository.add(user.getId(), USER_PROJECT);
            Assert.assertNotNull(projectRepository.findById(USER_PROJECT.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findByLogin() throws LoginEmptyException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final UserDTORepository repository = new UserDTORepository(entityManager);
            Assert.assertNotNull(repository.findByLogin(LOGIN_TEST));
            entityManager.close();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Test
    public void removeById() throws AbstractFieldException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final UserDTORepository repository = new UserDTORepository(entityManager);
            repository.removeById(userTesting.getId());
            Assert.assertNull(repository.findByLogin(LOGIN_TEST));
            entityManager.close();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
